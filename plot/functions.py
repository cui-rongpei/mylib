import numpy as np
import matplotlib.pyplot as plt

# 定义 ReLU 函数
def ReLU(x):
    return np.maximum(0, x)

# 定义 ReLU 函数的导数
def dReLU(x):
    return np.where(x < 0, 0, 1)

def LeakyReLU(x, k=0.2):
    return np.where(x >= 0, x, x * k)

def dLeakyReLU(x, k=0.2):
    return np.where(x >= 0, 1., k)

def Tanh(x):
    return np.tanh(x)

def dTanh(x):
    return 1. - Tanh(x)**2

def Sigmoid(x):
    return 1. / (1. + np.exp(-x))

def dSigmoid(x):
    return Sigmoid(x) * (1. - Sigmoid(x))

# 定义一个通用的绘图函数
def plot_function_and_derivative(func, derivative_func, x_range, filename, figsize=(12, 6), title='Function and Derivative Plot', xlabel='x', ylabel='y'):
    # 生成 x 轴的值
    x = np.linspace(x_range[0], x_range[1], 400)
    
    # 计算 y 轴的值
    y = func(x)
    dy = derivative_func(x)
    
    # 创建一个图形并设置子图
    fig, ax = plt.subplots(1, 2, figsize=figsize)
    
    # 绘制原函数图像
    ax[0].plot(x, y, label=func.__name__, color='blue')
    ax[0].set_title(f'{title} - {func.__name__}')
    ax[0].set_xlabel(xlabel)
    ax[0].set_ylabel(ylabel)
    ax[0].grid(True)
    ax[0].legend()
    
    # 绘制导数函数图像
    ax[1].plot(x, dy, label=f'd{func.__name__}', color='red')
    ax[1].set_title(f'{title} - d{func.__name__}')
    ax[1].set_xlabel(xlabel)
    ax[1].set_ylabel(f'd{ylabel}')
    ax[1].grid(True)
    ax[1].legend()
    
    # 保存图像
    plt.tight_layout()
    plt.savefig(filename)

# 定义一个通用的绘图函数
def plot_function(func, x_range, filename, figsize=(8, 6), title='Function Plot', xlabel='x', ylabel='y'):
    # 生成 x 轴的值
    x = np.linspace(x_range[0], x_range[1], 400)
    
    # 计算 y 轴的值
    y = func(x)
    
    # 绘制图像
    plt.figure(figsize=figsize)
    plt.plot(x, y, label=func.__name__, color='blue')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid(True)
    plt.legend()
    
    # 保存图像
    plt.savefig(filename)
